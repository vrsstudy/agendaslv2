 <?php 
  session_start();
  require 'sistema/inicialize.php';

  

  if(isset($_REQUEST['codsetor'])){
    $codsetor = $_REQUEST['codsetor'];
  }else{
    header("location: index.php");
  }

  if(isset($_REQUEST['logout'])){
    session_destroy();
    echo "<script>window.history.back();</script>";
  }

  if(isset($_SESSION['login']) && $_SESSION['login'] = "hakunamatata"){
    $admin = true;
  }else{
    $admin = false;
  }


  if(isset($_REQUEST['excluir-ramal'])){
    $codColaboradorExcluir = $_REQUEST['excluir-ramal'];
    $excluir = DBDelete('ramais', 'id = '.$codColaboradorExcluir.'');
    if($excluir){
      $checkRamais = DBCount('ramais', "WHERE codsetor = ".$codsetor."");
      if($checkRamais <= 0){
        $validar = array(
          'status' => '0'
        );
        $validarSETOR = DBUpdate('agenda', $validar, 'codsetor = '.$codsetor.'');
        echo "<script>window.history.back();</script>";
      }else{
        echo "<script>window.history.back();</script>";
      }
    }
  }

 ?>
 <!DOCTYPE html>
  <html>
    <head>
      <title>AgendaSL</title>
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
      <link rel="icon" href="imagens/favicon-sl.ico" type="image/x-icon" />
      <style type="text/css">
        .tabela-info{
          table-layout: fixed;
          width: 100%;SS
          background: #fffff;
          border: 2px solid #ccc;
          margin: 20px 0;
          margin-bottom: 40px;
        }
     
        .icone-visualizar{
          padding: 10px;
          color: #fff;
          cursor: pointer;
          border-radius: 10px;
        }
      </style>


      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body> 

      <nav>
        <div class="nav-wrapper  blue darken-1">
          <a href="index.php" class="brand-logo center">AgendaSL</a>
          <ul id="nav-mobile" class="left">
          <li><?php if($admin){echo '<a href="?logout=logout"><i class="material-icons left">exit_to_app</i></a></a>';}else
        {echo '<a href="?admin=admin"><i class="material-icons left">settings</i></a>';} ?></li>
          </ul>
        </div>
      </nav>


      <div class="container">
        <div class="row">
          <table class="striped tabela-info">
            <thead>
              <tr>
                <th>Setor</th>
                <th>E-mail</th>
                <th>Unidade</th>
              </tr>
            </thead>

            <tbody>

              <?php
                $codsetor = $_REQUEST['codsetor'];
                $registrosSetor = DBCount('agenda', "WHERE codsetor = $codsetor");
                if($registrosSetor > 0){
                  $setor = DBReadOne('agenda',  "WHERE codsetor = $codsetor", 'setor, email, unidade');
              ?>
              <tr>
                <td><?php echo $setor[0]; ?></td>
                <td><?php echo $setor[1]; ?></td>
                <td><?php echo $setor[2]; ?></td>
              </tr>
              <?php } ?>

            </tbody>
          </table>

          <?php if($admin){ 
            echo '<button id="btn-cadastrar-ramal" style="margin-bottom: 20px;" class=" blue lighten-1 waves-effect waves-light btn"><i class="material-icons left">add</i>Ramal</button>';
            } 
          ?>

          <div class="row" id="cadastrar-ramal" style="display: none; padding-right: 15px;">

            <?php

              if(isset($_POST['cadastrar']) && $admin){
                $colaborador   = $_POST['colaborador'];
                $ramal         = $_POST['ramal'];

              
                $ramal = array(
                  'codsetor'       => $codsetor,
                  'colaborador'    => $colaborador,
                  'numero'         => $ramal
                );

                $cadastrar = DBCreate('ramais', $ramal);
                if($cadastrar){
                  
                  $validar = array(
                    'status' => '1'
                  );

                  $validarSETOR = DBUpdate('agenda', $validar, 'codsetor = '.$codsetor.'');


                  if($validarSETOR){
                    echo "<script>Swal.fire({title: 'Sucesso!',
                      text: 'Ramal cadastrado.',
                      type: 'success',
                      confirmButtonText: 'Ok'}).then((result) => {if (result.value) { 
                           window.location.replace('?codsetor=".$codsetor."'); }});</script>";
                  }
                }
              }
              

            ?>
            <form method="post">         
      
              <div class="input-field col s4">
                <input  id="colaborador" name="colaborador" type="text" class="validate" required>
                <label for="colaborador">Colaborador</label>
              </div>

              <div class="input-field col s4">
                <input id="ramal" name="ramal" type="text" class="validate" required>
                <label for="ramal">Ramal</label>
              </div>

              <button class="btn waves-effect waves-light green lighten-1 col s2 " style="margin-top: 20px; margin-left: 20px;" type="submit" name="cadastrar">Salvar
                <i class="material-icons right">save</i>
              </button>

            </form>
          </div>

          <table class="striped" style="border: 2px solid #ccc;">
            <thead>
              <tr>
                <th>Colaborador</th>
                <th>Ramal</th>
                <?php if($admin){echo "<th></th>";} ?>
              </tr>
            </thead>

            <tbody>

              <?php
                $codsetor = $_REQUEST['codsetor'];
                $registrosRamais = DBCount('ramais', "WHERE codsetor = $codsetor");
                if($registrosRamais > 0){
                  $ramais = DBRead('ramais',  "WHERE codsetor = $codsetor", 'id, numero, colaborador');
                  foreach ($ramais as $ramal) {
              ?>
              <tr class="ramais-tr" data-codcolaborador ='<?php echo $ramal["id"]; ?>'>
                <td><?php echo $ramal['colaborador']; ?></td>
                <td><?php echo $ramal['numero']; ?></td>
                <?php if($admin){echo '
                <td>
                  <a class="excluir-ramal" href="?codsetor='.$codsetor.'&excluir-ramal='.$ramal["id"].'" style="float: right; margin-right:20px;" ><i class="icone-visualizar material-icons red lighten-1 ">delete</i></a>

                  <a class="editar-colaborador"  style="float: right; margin-right:20px;" ><i class="icone-visualizar material-icons green lighten-1 ">edit</i></a>

                  <a class="salvar-colaborador"  style="float: right; margin-right:20px; display: none;" ><i class="icone-visualizar material-icons light-green darken-3">done_outline</i>
                  </a>
                </td>'
                ;} ?>
              </tr>
              <?php }}else{
                echo "<td>Nenhum ramal cadastrado.</td><td></td>";
                if($admin){echo "<td></td>";}
              } ?>

            </tbody>
          </table>
        </div>
      </div>
     

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

      <script type="text/javascript">
        $("document").ready(function(){

          function liberarEdicao(){
            $(".editar-colaborador").on("click", function(){

              $('.ramais-tr').hide();
              $(this).parent().parent().show();

              $(this).hide().parent().find('.salvar-colaborador').show().parent().find('.excluir-ramal').hide();

              var codColaborador = $(this).parent().parent().attr("data-codcolaborador");
              var colaborador    = $(this).parent().siblings(":first").text();
              var ramal          = $(this).parent().siblings(":nth-child(2)").text();

              
              $(this).parent().siblings(":first").html('<input  class="colaborador-edit"  placeholder="colaborador" type="text" value="'+colaborador+'">');

              $(this).parent().siblings(":nth-child(2)").html('<input class="ramal-edit" placeholder="ramal" type="text" value="'+ramal+'">');
            });
          }

          function salvarLinhas(){
            $(".salvar-colaborador").on("click", function(){
              var codColaborador = $(this).parent().parent().attr("data-codcolaborador");
              var colaborador    = $(".colaborador-edit").val();
              var ramal          = $(".ramal-edit").val(); 

              if (colaborador == '' || ramal == ''){
                Swal.fire({
                  title: 'Erro!',
                  text: 'Preencha os campos!',
                  type: 'error',
                  confirmButtonText: 'Ok'
                })
              }else{

                $.ajax({url: 'editar-ramal.php?codColaborador='+codColaborador,
                  type: 'GET',
                  data: {
                    'colaborador': colaborador,
                    'ramal' : ramal
                  }
                }).done(function(data){
                  if(data = "true"){

                    Swal.fire({
                      title: 'Sucesso!',
                      text: 'Ramal editado.',
                      type: 'success',
                      confirmButtonText: 'Ok'
                    }).then((result) => {
                      if (result.value) {    location.reload() }
                    });

                  }
                });
              
              }

            });
          }

          function excluirRamal(){

          }

          liberarEdicao();
          salvarLinhas();

          $("#btn-cadastrar-ramal").on("click", function(){
            $("#cadastrar-ramal").slideToggle();
          });

          

        });
      </script>
    </body>
  </html>