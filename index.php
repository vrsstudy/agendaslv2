 <?php 
  session_start();
  require 'sistema/inicialize.php';

  if(isset($_REQUEST['logout'])){
    session_destroy();
    header("Location: index.php");
  }
  if(isset($_REQUEST['admin'])){
    session_destroy();
    header("Location: admin.php");
  }
  if(isset($_SESSION['login']) && $_SESSION['login'] = "hakunamatata"){
    $admin = true;
  }else{
    $admin = false;
  }


  if(isset($_REQUEST['exclusao'])){
    $codSetorExclusao = $_REQUEST['codsetor'];
    $exclusao_ramais = DBDelete('ramais', 'codsetor = '.$codSetorExclusao.'');
    if($exclusao_ramais){
      $exclusao = DBDelete('agenda', 'codsetor = '.$codSetorExclusao.'');
      if($exclusao){
        echo "<script>  window.location.replace('index.php');</script>";
      }
    }
  }

 ?>
 <!DOCTYPE html>
 <html>
 <head>
  <title>AgendaSL</title>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
  <link rel="icon" href="imagens/favicon-sl.ico" type="image/x-icon" />
  <style type="text/css">
  .icone-visualizar{
    padding: 10px;
    color: #fff;
    cursor: pointer;
    border-radius: 5px;
  }
  .mdl-button--raised.mdl-button--colored{
    background: #42a5f5 !important;

  }
</style>


<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>

  <nav>
    <div class="nav-wrapper   blue darken-1">
      <a href="index.php" class="brand-logo center">AgendaSL</a>
      <ul id="nav-mobile" class="left">
        <li><?php if($admin){echo '<a href="?logout=logout"><i class="material-icons left">exit_to_app</i></a></a>';}else
        {echo '<a href="?admin=admin"><i class="material-icons left">settings</i></a>';} ?></li>
      </ul>
    </div>
  </nav>


  <div class="container">
    <div class="row" style="padding-top: 20px;">
    <?php if($admin){ 
      echo '<a href="add_setor.php" class="  blue darken-1 waves-effect waves-light btn"><i class="material-icons left">add</i>Setor</a>';
      } 
    ?>
     <table id="example" class="mdl-data-table" style="width:100%">
      <thead>
        <tr>
          <th>Setor</th>
          <th>E-mail</th>
          <th>Unidade</th>
          <th>Ramais</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $registrosAgenda = null;
        if($admin){
          $registrosAgenda = DBCount('agenda', "");
          $setores = DBRead('agenda',  "", 'codsetor, setor, email, unidade');
        }else{
          $registrosAgenda = DBCount('agenda', "WHERE status = 1");
          $setores = DBRead('agenda',  "WHERE status = 1", 'codsetor, setor, email, unidade');
        }
        if($registrosAgenda > 0){
          foreach ($setores as $setor) {
        ?>
          <tr>
            <td><?php echo $setor['setor']; ?></td>
            <td><?php echo $setor['email']; ?></td>
            <td><?php echo $setor['unidade']; ?></td>
            <td>
              <?php if($admin){
                echo '
                <a href="editar_setor.php?codsetor='.$setor['codsetor'].'"><i class="icone-visualizar material-icons light-green darken-1">edit</i></a>

                <a  href="?excluir-setor='.$setor["codsetor"].'"  ><i class="icone-visualizar material-icons red darken-1 ">delete</i></a>

                ';
              } ?>
              <a href="exibir_ramais.php?codsetor=<?php echo $setor['codsetor']; ?>"><i class="icone-visualizar  blue darken-1 material-icons">visibility</i></a>
            </td>
          </tr>
        <?php }} ?>

      </tbody>
    </table>
  </div>
</div>


<!-- popup exclusao setor-->
<?php 
  if(isset($_REQUEST['excluir-setor'])){
      $codsetor = $_REQUEST['excluir-setor'];
      echo "<script>Swal.fire(
              'Exclusão',
              'Você deseja excluir o setor?',
              'question'
            ).then((result) => {if (result.value) {    window.location.replace('?codsetor=".$codsetor."&exclusao=');  }});</script>";
  }
?>


<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#example').DataTable( {
      columnDefs: [
      {
        targets: [ 0, 1, 2 ],
        className: 'mdl-data-table__cell--non-numeric'
      }
      ]
    });

    $("#example_info").hide();
    $("#example_length").hide();


  });
</script>

</body>
</html>