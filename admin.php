<?php
session_start();
if (isset($_SESSION['login'])) {
  echo "<script>location.replace('index.php')</script>";
}
?>
<html>

<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="icon" href="imagens/favicon-sl.ico" type="image/x-icon" />
  <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
  <link rel="stylesheet" href="css\admin.css">
</head>

<body>
  <form class="form-signin" method="POST">
    <div align="center">
      <img src="imagens/logo-saolucas-branca.svg" align="center" position="fixed">
    </div>
    <div class="login-box">
      <h1>Entrar</h1>
      <div class="textbox">
        <i class="fa fa-user"></i>
        <input type="text" name="login" placeholder="Usuário">
      </div>

      <div class="textbox">
        <i class="fa fa-lock"></i>
        <input type="password" name="password" placeholder="Senha">
      </div>
      <button class="btn btn-lg btn-primary btn-block" name="btn_login" type="submit">Login</button>
    </div>
  </form>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

  <?php
  if (isset($_POST['btn_login'])) {
    $login_setado = 'admin';
    $senha_setado = "inform3tic3";

    $login = $_POST['login'];
    $senha = $_POST['password'];

    if ($login == $login_setado && $senha == $senha_setado) {
      $_SESSION['login'] = 'hakunamatata';
      echo "<script>location.replace('index.php')</script>";
    } else {
      echo "<script>
        Swal.fire({title: 'Ops!',
        text: 'Login ou Senha invalidos!.',
        type: 'error',
        confirmButtonText: 'Ok'})
        </script>";
    }
  }
  ?>
</body>

</html>