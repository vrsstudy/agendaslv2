 <?php 
  session_start();

  if (!isset($_SESSION['login'])){
    header("Location: index.php");
  }

  require 'sistema/inicialize.php';

  if(isset($_REQUEST['logout'])){
    session_destroy();
    header("Location: index.php");
  }

  if(isset($_SESSION['login']) && $_SESSION['login'] = "hakunamatata"){
    $admin = true;
  }else{
    $admin = false;
  }

 ?>
 <!DOCTYPE html>
 <html>
 <head>
  <title>AgendaSL</title>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
  <link rel="icon" href="imagens/favicon-sl.ico" type="image/x-icon" />
  <style type="text/css">
    .swal2-popup .select-wrapper{
      display: none;
    }
  </style>
  


<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>

  <nav>
    <div class="nav-wrapper   blue darken-1">
      <a href="index.php" class="brand-logo center">AgendaSL</a>
      <ul id="nav-mobile" class="left">
    <li><?php if($admin){echo '<a href="?logout=logout"><i class="material-icons left">exit_to_app</i></a></a>';}else
        {echo '<a href="?admin=admin"><i class="material-icons left">settings</i></a>';} ?></li>
      </ul>
    </div>
  </nav>


  <div class="container">
    <div class="row" style="padding-top: 20px;">
      <h5 style="background: #e1e2e3;padding: 10px; border-radius: 8px;">CADASTRO DE SETOR</h5>

      <?php

        if(isset($_POST['cadastrar'])){
          $setor   = $_POST['setor'];
          $email   = $_POST['email'];
          $unidade = $_POST['unidade'];

          $checkarSeExiste = DBCount('agenda', "WHERE email = '$email'");
          if($checkarSeExiste > 0){
            echo "<script>Swal.fire({title: 'Erro!',text: 'Setor já cadastrado.',type: 'error',confirmButtonText: 'Ok'});</script>";
          }else{
            $setor = array(
              'setor'    => $setor,
              'email'    => $email,
              'unidade'  => $unidade,
              'status'   => '0'
            );
            $cadastrar = DBCreate('agenda', $setor);
            if($cadastrar){

              $getId = DBReadOne('agenda',  "WHERE email = '$email'", 'codsetor');
              $id  = $getId[0];

              echo "<script>Swal.fire({title: 'Sucesso!',text: 'Setor cadastrado.',type: 'success',confirmButtonText: 'Ok'}).then((result) => {if (result.value) {    window.location.replace('exibir_ramais.php?codsetor=".$id."'); }});</script>";
            }
          }
        }

      ?>

      <form method="post">

        <div class="input-field col s6">
          <input id="setor" name="setor" type="text" class="validate" required="">
          <label for="setor">Setor</label>
        </div>

        <div class="input-field col s6">
          <input id="email" name="email" type="text" class="validate" required="">
          <label for="email">E-mail</label>
        </div>

        <div class="input-field col s12">
          <select name="unidade" required="">
            <option value="" disabled selected>Selecione</option>
            <option value="São Lucas - Unidade I">São Lucas - Unidade I</option>
            <option value="Centro de Serviços Compartilhados - CSC">Centro de Serviços Compartilhados - CSC</option>
          </select>
          <label>Unidade</label>
        </div>
        <button class="btn waves-effect waves-light green lighten-1" type="submit" name="cadastrar">Salvar
          <i class="material-icons right">save</i>
        </button>
      </form>
  </div>
</div>


<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('select').formSelect();
  });
</script>
</body>
</html>