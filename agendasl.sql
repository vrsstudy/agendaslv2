-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 04-Jul-2019 às 21:40
-- Versão do servidor: 5.7.26
-- versão do PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agendasl`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ag_agenda`
--

DROP TABLE IF EXISTS `ag_agenda`;
CREATE TABLE IF NOT EXISTS `ag_agenda` (
  `codsetor` int(11) NOT NULL AUTO_INCREMENT,
  `setor` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `unidade` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`codsetor`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ag_agenda`
--

INSERT INTO `ag_agenda` (`codsetor`, `setor`, `email`, `unidade`, `status`) VALUES
(1, 'NDT', 'suportesistemas@saolucas.edu.br', 'sede', 1),
(2, 'NUC', 'nuc@saolucas.edu.br', 'sede', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ag_ramais`
--

DROP TABLE IF EXISTS `ag_ramais`;
CREATE TABLE IF NOT EXISTS `ag_ramais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codsetor` varchar(255) NOT NULL,
  `numero` varchar(15) NOT NULL,
  `colaborador` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ag_ramais`
--

INSERT INTO `ag_ramais` (`id`, `codsetor`, `numero`, `colaborador`) VALUES
(1, '1', '8148', 'vinicius'),
(2, '1', '8146', 'teste');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
